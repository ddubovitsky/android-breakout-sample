package me.shpp.ddubovitsky.lolol.views.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Dubovitsky Denis on 7/8/2017.
 */

public class Ball {
    private static final int BALL_SPEED = 9;

    public static int BALL_RADIUS = 25;

    private int ballDx = BALL_SPEED;
    private int ballDy = -BALL_SPEED;


    private int ballXPosition;
    private int ballYPosition;

    private int backgroundHeight;
    private int backgroundWidth;

    private Paint ballPaint;

    public Ball(int backgroundWidth, int backgroundHeight) {
        ballPaint = new Paint();
        ballPaint.setStyle(Paint.Style.FILL);
        ballPaint.setAntiAlias(true);
        ballPaint.setColor(Color.WHITE);

        this.backgroundHeight = backgroundHeight;
        this.backgroundWidth = backgroundWidth;

        ballXPosition = backgroundWidth / 2;
        ballYPosition = backgroundHeight / 2;
    }

    public void draw(Canvas canvas) {
        ballXPosition += ballDx;
        ballYPosition += ballDy;
        canvas.drawCircle(ballXPosition, ballYPosition, BALL_RADIUS, ballPaint);
    }

    public void reverseDx() {
        ballDx = -ballDx;
    }

    public void reverseDy() {
        ballDy = -ballDy;
    }

    public void collide(Paddle gamePaddle) {
        if (ballYPosition + BALL_RADIUS > backgroundHeight || ballYPosition - BALL_RADIUS < 0) {
            ballDy = -ballDy;
            return;
        }

        if (ballXPosition + BALL_RADIUS > backgroundWidth || ballXPosition - BALL_RADIUS < 0) {
            ballDx = -ballDx;
            return;
        }

        if (gamePaddle.contains(ballXPosition, ballYPosition + BALL_RADIUS)) {
            ballDy = -ballDy;
            return;
        }

        if (gamePaddle.contains(ballXPosition + BALL_RADIUS, ballYPosition + BALL_RADIUS)) {
            ballDx = -ballDx;
            return;
        }

        if (gamePaddle.contains(ballXPosition - BALL_RADIUS, ballYPosition + BALL_RADIUS)) {
            ballDx = -ballDx;
        }
    }

    public int getBallXPosition() {
        return ballXPosition;
    }

    public void setBallXPosition(int ballXPosition) {
        this.ballXPosition = ballXPosition;
    }

    public int getBallYPosition() {
        return ballYPosition;
    }

    public void setBallYPosition(int ballYPosition) {
        this.ballYPosition = ballYPosition;
    }

}
