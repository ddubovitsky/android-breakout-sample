package me.shpp.ddubovitsky.lolol.views.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

/**
 * Created by Dubovitsky Denis on 7/8/2017.
 */

public class Block {

    private static final int BLOCK_WIDTH_RATIO = 10;
    private static final int BLOCK_HEIGHT_RATIO = 12;

    private int left;
    private int width;
    private int top;
    private int height;

    private Rect blockRect;

    private Paint blockPaint;

    private boolean isOnScreen;

    public Block(int position, int backgroundWidth, int backgroundHeight) {
        width = backgroundWidth / BLOCK_WIDTH_RATIO;
        left = position * width;
        width -= 10;
        top = 10;
        height = backgroundHeight / BLOCK_HEIGHT_RATIO;

        blockPaint = new Paint();
        blockPaint.setStyle(Paint.Style.FILL);
        blockPaint.setAntiAlias(true);
        blockPaint.setColor(Color.WHITE);
        isOnScreen = true;
        blockRect = new Rect();

    }

    public void draw(Canvas canvas) {
        blockRect.set(left, top, left + width, top + height);
        canvas.drawRect(blockRect, blockPaint);
    }


    public void collide(Ball gameBall) {
        boolean collides = blockRect.contains(gameBall.getBallXPosition(), gameBall.getBallYPosition() - Ball.BALL_RADIUS) && isOnScreen;

        if (collides) {
            gameBall.reverseDy();
            isOnScreen = false;
        }
    }

    public boolean isOnScreen() {
        return isOnScreen;
    }

    public void setOnScreen(boolean onScreen) {
        isOnScreen = onScreen;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
