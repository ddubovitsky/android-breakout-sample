package me.shpp.ddubovitsky.lolol.views.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

/**
 * Created by Dubovitsky Denis on 7/8/2017.
 */

public class Paddle {
    private static final int PADDLE_RATIO = 9;

    private int paddleStart;
    private int paddleEnd;
    private Rect paddleRect;
    private int paddleWidth;
    private Paint mPaddlePaint;

    private int paddleTop;
    private int paddleBottom;
    private int paddleDx;


    public Paddle(int backgroundWidth, int backgroundHeight) {
        mPaddlePaint = new Paint();
        mPaddlePaint.setStyle(Paint.Style.FILL);
        mPaddlePaint.setAntiAlias(true);
        mPaddlePaint.setColor(Color.WHITE);

        paddleWidth = backgroundWidth / PADDLE_RATIO;
        paddleStart = 0;
        paddleEnd = paddleStart + paddleWidth;
        paddleTop = backgroundHeight - 50;
        paddleBottom = backgroundHeight;

        paddleRect = new Rect(paddleStart, paddleTop, paddleEnd, paddleBottom);
    }

    public void draw(Canvas canvas) {
        updateRect();
        Log.d("+++", "Drawing");
        Log.d("+++", paddleRect.left + " " + paddleRect.top + " " + paddleRect.right + " " + paddleRect.bottom);
        canvas.drawRect(paddleRect, mPaddlePaint);
    }

    private void updateRect() {
        paddleStart += paddleDx;
        paddleEnd = paddleStart + paddleWidth;
        paddleRect.set(paddleStart, paddleTop, paddleEnd, paddleBottom);
    }

    public void setDx(int dx) {
        paddleDx = dx;
    }

    public boolean contains(int x, int y) {
        return paddleRect.contains(x, y);
    }
}
