package me.shpp.ddubovitsky.lolol.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

import me.shpp.ddubovitsky.lolol.views.models.Ball;
import me.shpp.ddubovitsky.lolol.views.models.Block;
import me.shpp.ddubovitsky.lolol.views.models.Paddle;

/**
 * Created by Dubovitsky Denis on 7/8/2017.
 */

public class GameView extends View {

    private static final String TAG = GameView.class.getSimpleName();

    private int width;
    private int height;

    private Paint mWhitePaint, mBlackPaint;

    private Paddle gamePaddle;

    private Ball gameBall;

    ArrayList<Block> mBlocks;

    public GameView(Context context) {
        super(context);
        init();
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mBlackPaint = new Paint();
        mBlackPaint.setStyle(Paint.Style.FILL);
        mBlackPaint.setAntiAlias(true);
        mBlackPaint.setColor(Color.BLACK);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBlocks = new ArrayList<>();
        width = getMeasuredWidth();
        height = getMeasuredHeight();

        gameBall = new Ball(width, height);
        gamePaddle = new Paddle(width, height);

        for (int i = 0; i < 9; i++) {
            mBlocks.add(new Block(i, width, height));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(0, 0, width, height, mBlackPaint);
        gamePaddle.draw(canvas);
        gameBall.draw(canvas);
        gameBall.collide(gamePaddle);

        for (Block b : mBlocks) {
            b.collide(gameBall);

            if (b.isOnScreen())
                b.draw(canvas);
        }
    }

    public void update() {
        invalidate();
    }

    public void setPaddleDx(int paddleDx) {
        if (gamePaddle == null)
            return;

        gamePaddle.setDx(paddleDx);
    }

}
